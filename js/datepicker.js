function populateDays(month) {
    const daySelect = document.querySelector('[name=day]');
    const month = monthSelect.value;

    let dayNum;

    // 31 or 30 days?
    switch (month) {
        case 'April': case 'June': case 'September': case 'November':
        dayNum = 30;
        break;
        case 'February':
            // If month is February, calculate whether it is a leap year or not
            const year = yearSelect.value;
            const isLeap = new Date(year, 1, 29).getMonth() === 1;
            dayNum = isLeap ? 29 : 28;
            break;
        default:
            dayNum = 31;
    }

    daySelect.options = Array.from({ length: dayNum }, function(index) {
        return index + 1;
    });


    if (previousDay) {
        daySelect.value = previousDay;

        if (previousDay > daySelect.length + 1) {
            daySelect.selectedIndex = daySelect.length;
        }
    }
}

yearSelect.onchange = populateDays;
monthSelect.onchange = populateDays;

// preserve day selection
var previousDay;

daySelect.onchange = function() {
    previousDay = daySelect.value;
};